# FrontEnd Bank Account Challenge

## Development Build

1. `npm install`
2. `npm start`

## Production Build

1. `npm run build`

## Github
git clone https://modimrugesh1910@bitbucket.org/modimrugesh1910/vimeo.git

## Activities Done - 

I have covered below details -

* fetching data over http call
* sharing data over files
* Implemented pagination on the frontend. Display 10 records max in a page.
* Visually interactive design to list​ details.
* Implemented a Search functionality to search transaction details for a particular recipient.
* Implemented charts
* version control
* deployment of code
