export const menus = [
    {
        'name': 'List',
        'icon': 'list',
        'link': '/auth/table',
        'open': false,
    }, {
        'name': 'Chart',
        'icon': 'bar_chart',
        'link': '/auth/chart',
        'open': false,
    }
];

