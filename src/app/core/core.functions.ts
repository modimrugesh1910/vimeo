import * as moment from 'moment';
export const convertDate = (date): string => {
    return moment(date).format('DD-MMM-YYYY');
};

