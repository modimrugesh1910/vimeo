import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ChartComponent} from './chart.component';

const moduleRoutes: Routes = [
    {path: '', component: ChartComponent, data: {animation: 'chart'}},
];

@NgModule({
    imports: [
        RouterModule.forChild(moduleRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ChartRouterModule {
}
