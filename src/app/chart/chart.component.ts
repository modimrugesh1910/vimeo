import {AfterViewInit, Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import * as moment from 'moment';
import {CommonService} from '../core/common.service';

@Component({
    selector: 'app-dashboard-chart',
    templateUrl: './chart.component.html',
    styleUrls: ['./chart.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ]
})

export class ChartComponent implements OnInit, AfterViewInit {
    rawData: Array<any> = [];

    colorScheme = {
        domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
    };
    chartData: Array<any> = [];
    viewDimention: any[] = [1050, 450];
    xAxisLabel = 'Date';
    yAxisLabel = 'Balance Amount (Lacs)';
    chartArray = [
        {
            'key': 'Withdrawal_AMT',
            'value': 0,
            'name': 'Withdraw',
            'title': 'Withdraw Amount (in Lacs)',
        }, {
            'key': 'Deposit_AMT',
            'value': 1,
            'name': 'Deposit',
            'title': 'Deposit Amount (in Lacs)'
        }, {
            'key': 'Balance_AMT',
            'value': 2,
            'name': 'Balance',
            'title': 'Balance Amount (in Lacs)'
        }
    ];
    selectedChart = this.chartArray[2].name;


    constructor(private readonly commonService: CommonService) {
    }

    ngOnInit(): void {
        this.loadData();
    }

    ngAfterViewInit(): void {
    }

    loadData(): void {
        this.commonService.fetchData().subscribe((res) => {
            res.map(function (item) {
                item['Account_No'] = item['Account No'];
                item['Transaction_Details'] = item['Transaction Details'];
                item['Value_Date'] = item['Value Date'];
                item['Withdrawal_AMT'] = (item['Withdrawal AMT'] === '' ? 0 : parseInt(item['Withdrawal AMT'], 10));
                item['Deposit_AMT'] = (item['Deposit AMT'] === '' ? 0 : parseInt(item['Deposit AMT'], 10));
                item['Balance_AMT'] = (item['Balance AMT'] === '' ? 0 : parseInt(item['Balance AMT'], 10));
                delete item['Account No'];
                delete item['Transaction Details'];
                delete item['Value Date'];
                delete item['Withdrawal AMT'];
                delete item['Deposit AMT'];
                delete item['Balance AMT'];
                return item;
            });
            this.rawData = res;
            this.processData(res, 2);
        });
    }

    changeChart(chartId) {
        this.processData(this.rawData, chartId);
            this.yAxisLabel = this.chartArray[chartId].title;
        }

    private processData(rawData, chartId) {
        const temp = {
            schemeName: '',
            data: [],
        };

        for (const row of rawData) {
            temp.data.push({
                'name': row.Date,
                'value': row[this.chartArray[chartId].key]
            });
        }

        this.chartData = [{
            name: temp.schemeName,
            series: temp.data
        }];
        Object.assign({}, this.chartData);
    }
}
