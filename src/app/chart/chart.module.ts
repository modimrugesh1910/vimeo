import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material';
import {MaterialModule} from '../shared/angular-material.module';
import {CoreModule} from '../core/core.module';
import {ChartComponent} from './chart.component';
import {ChartRouterModule} from './chart.routes';
import {NgxChartsModule} from '@swimlane/ngx-charts';

@NgModule({
    imports: [
        CommonModule,
        FlexLayoutModule,
        MaterialModule,
        ReactiveFormsModule,
        FormsModule,
        CoreModule,
        NgxChartsModule,
        ChartRouterModule
    ],
    declarations: [
        ChartComponent
    ],
    exports: [],
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'en-GB'}
    ]
})
export class ChartModule {
}


