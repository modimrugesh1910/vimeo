import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';

import {TablesRouterModule} from './tables.router';

import {MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material';

import {TableComponent} from './table.component';
import {MaterialModule} from '../shared/angular-material.module';
import {CoreModule} from '../core/core.module';
import {TableService} from './tables.service';

@NgModule({
    imports: [
        CommonModule,
        FlexLayoutModule,
        MaterialModule,
        ReactiveFormsModule,
        FormsModule,
        CoreModule,
        TablesRouterModule
    ],
    declarations: [
        TableComponent
    ],

    exports: [],
    providers: [
        TableService,
        {provide: MAT_DATE_LOCALE, useValue: 'en-GB'}
    ]
})
export class TablesModule {
}


