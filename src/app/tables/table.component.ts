import {
    AfterViewInit,
    Component,
    ElementRef,
    OnInit,
    ViewChild
} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {fromEvent as observableFromEvent, BehaviorSubject} from 'rxjs';
import {distinctUntilChanged, debounceTime, switchMap} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CommonService} from '../core/common.service';

@Component({
    selector: 'app-watchlist-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ]
})

export class TableComponent implements OnInit, AfterViewInit {
    dataSource: any;
    fundTableForm: FormGroup;
    headerNames: Array<string> = ['Account_No', 'Date', 'Transaction_Details', 'Value_Date', 'Withdrawal_AMT', 'Deposit_AMT', 'Balance_AMT'];
    columnsToDisplay: any;
    fundDataSource: any;
    selectedData: any;

    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild('filter', {static: false}) filter: ElementRef;

    constructor(private readonly commanService: CommonService, private formBuilder: FormBuilder) {
    }

    ngOnInit(): void {
        this.fundTableForm = this.formBuilder.group({
            fundSchemeName: new FormControl('', Validators.required)
        });
    }

    ngAfterViewInit(): void {
        this.commanService.fetchData().subscribe((res) => {
            res.map(function (item) {
                item['Account_No'] = item['Account No'];
                item['Transaction_Details'] = item['Transaction Details'];
                item['Value_Date'] = item['Value Date'];
                item['Withdrawal_AMT'] = item['Withdrawal AMT'];
                item['Deposit_AMT'] = item['Deposit AMT'];
                item['Balance_AMT'] = item['Balance AMT'];
                delete item['Account No'];
                delete item['Transaction Details'];
                delete item['Value Date'];
                delete item['Withdrawal AMT'];
                delete item['Deposit AMT'];
                delete item['Balance AMT'];
                return item;
            });
            this.dataSource = new MatTableDataSource(res);
            this.columnsToDisplay = res;
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;

            observableFromEvent(this.filter.nativeElement, 'keyup').pipe(
                debounceTime(150),
                distinctUntilChanged(), )
                .subscribe(() => {
                    if (!this.dataSource) {
                        return;
                    }
                    this.dataSource.filter = this.filter.nativeElement.value;
                });
        });
    }

    private getFundRawData({value, valid}: { value: any, valid: boolean }) {
        return this.fundTableForm.getRawValue();
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
}

