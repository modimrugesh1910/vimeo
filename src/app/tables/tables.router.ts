import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TableComponent} from './table.component';

const materialWidgetRoutes: Routes = [
  {path: '', component: TableComponent, data: {animation: 'watchlist'}},
];

@NgModule({
  imports: [
    RouterModule.forChild(materialWidgetRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TablesRouterModule {
}
